
## VECTO v4.2.7 Official Release (09-01-2025)

### Bug Fixes

- Track release_notes.md for release
- CodeEU #858: Convert steering pump tech (vecto/vecto!303)
- Converter Tool: ngTankSystem for dual fuel (vecto/vecto!304)
- CodeEU #836: Restrictions on IEPC gear and MaxTorqueCurve XSD attributes (vecto/vecto!302)