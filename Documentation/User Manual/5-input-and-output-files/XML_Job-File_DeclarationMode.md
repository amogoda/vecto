## XML Job-File (Declaration Mode)

<div class="declaration">
For vehicle certification the input data (vehicle data) has to be provided in XML format. Please see the following resources for more information:

* [XML schema description](VectoInputDeclaration.1.0.pdf)
* [XML schema diagram](../XML/XSD/VectoInput.xsd)
* [Example of an XML Job-File](../XML/Examples/vecto_vehicle-sample.xml)

</div>