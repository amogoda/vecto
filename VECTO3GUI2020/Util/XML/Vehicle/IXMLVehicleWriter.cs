﻿using System.Xml.Linq;

namespace VECTO3GUI2020.Util.XML.Vehicle
{
    public interface IXMLVehicleWriter
    {
        XElement GetElement();
    }
}
